import { Component } from "react";
import PropTypes from 'prop-types';
import Card from "../card"
import Modal from "../modal"
import "./list.scss"
import { useEffect, useState } from "react";
import ButtonAdd from "../button_add"
import ButtonFav from "../button_fav"

export default function List({onAdd,favAdd,favDel}) {

      const [item, setItem] = useState([]);
      const [isLoading,setIsLoading]=useState(true)
      const [modalToShow, setmodalToShow] = useState("");
      const [it, setIt] = useState("");
      const [favoricon,setFavoricon]=useState(localStorage.getItem("favoricon"))
  
  useEffect(() => {
    
      fetch("./list.json")
        .then((res) => res.json())
        .then((data) => {
                  setTimeout(() => {
                    setItem(data);
                    setIsLoading(false);
                  }, 1500);
                })
  })

      
    
    
           
              if (isLoading) {
                return(

                
                <div>loading...</div>)
     } else {
      return (
        <>
        
                <div className="list">
                    { item.map((el)=> (
                      <div className="list__card" key={el.id}>
          <Card key={el.id} name={el.name} image={el.image} 
          id={el.id}
        price={el.price}
        color={el.color}
        article={el.article}
                  


        


/>
<div className="card__button">
<ButtonAdd         Click={()=>{setmodalToShow(true) 

setIt(el)

}

}
/>
<ButtonFav         addDelete={(e)=>{
          if(e.target.parentNode.classList.contains('active')){
            e.target.parentNode.classList.remove('active')
            favDel(el.id)
        }else {
          e.target.parentNode.classList.add('active');
          favAdd(el)
        }
    
          
          }}/>
           </div>
           </div>

     
          ))}
          
           
         
         </div>
          <Modal
          question={"Ви хочете додати цей товар до корзини"} 
        
          active={modalToShow} 
          Click={(e)=> setmodalToShow(false)}
          add={()=>{onAdd(it)
            setmodalToShow(false)}}
           />
  
        </>
  
   
         )
              }
             

}