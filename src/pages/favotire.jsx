import Header from "../header";
import { useEffect, useState } from "react";
import Card from "../components/card"
import { FaTrash } from "react-icons/fa";
import "./favorite.scss"
import Modal from "../components/modal";
export default function Favorite (){
    const [item, setItems] = useState(JSON.parse(localStorage.getItem("cards"))?JSON.parse(localStorage.getItem("favorite")):[])
    const [modalToShow, setmodalToShow] = useState("");
    const [it, setIt] = useState("");
    let cart= JSON.parse(localStorage.getItem("cards"))

    function deleteFavorite(i){
        setItems(item.filter(el=>el.id!==i))
         
         
         }
 useEffect(()=>{localStorage.setItem("favorite", JSON.stringify(item))
 if(item==null){
     localStorage.removeItem("favorite")} } )    
     
   
        
    if (item!=null){
    return (
        <>
        <Header curent={cart?cart.length:0} favorites={item?item.length:0} c/>
    <div className="favorite">
    { item.map(el=> 
        <div className="favorite__card" key={Math.random()}>
<Card key={Math.random()}  name={el.name} image={el.image} 
price={el.price}
color={el.color}
article={el.article}/>

<div className="cart__fatrash"><FaTrash onClick={()=>{setmodalToShow(true) 

setIt(el.id)

}

} /></div>

</div>
)}

</div>
<Modal
          question={"Ви хочете видалити товар з обраного"} 
        
          active={modalToShow} 
          Click={(e)=> setmodalToShow(false)}
          add={()=>{deleteFavorite(it)
            setmodalToShow(false)}}
           />
</>)}
else {
    return (
        
        <Header curent={cart?cart.length:0} favorites={item?item.length:0} />)
}

 
}
