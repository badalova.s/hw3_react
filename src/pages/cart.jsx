
import Card from "../components/card"
import Header from "../header";
import { useEffect, useState } from "react";
import { FaTrash } from "react-icons/fa";
import "./cart.scss"
import Modal from "../components/modal";


export default function Cart (){
    const [item, setItems] = useState(JSON.parse(localStorage.getItem("cards"))?JSON.parse(localStorage.getItem("cards")):[])
    const [modalToShow, setmodalToShow] = useState("");
    const [it, setIt] = useState("");
    let favorite= JSON.parse(localStorage.getItem("favorite"))

    
        function deleteOrder(i){
           setItems(item.filter(el=>el.id!==i))
            
            
            }
    useEffect(()=>{localStorage.setItem("cards", JSON.stringify(item))
    if(item==null){
        localStorage.removeItem("cards")} } )    
        
        
    if (item!=null){
    return (
        <>
        <Header curent={item?item.length:0} favorites={favorite?favorite.length:0}/>
    <div className="cart">
    { item.map(el=> 
      
       <div className="cart__card" key={Math.random()}>
<Card key={Math.random()}  name={el.name} image={el.image} 
price={el.price}
color={el.color}
article={el.article}/>
<div className="cart__fatrash"><FaTrash onClick={()=>{setmodalToShow(true) 

setIt(el.id)

}

} /></div>

</div>
)}

</div>
<Modal
          question={"Ви хочете видалити товар з корзини"} 
        
          active={modalToShow} 
          Click={(e)=> setmodalToShow(false)}
          add={()=>{deleteOrder(it)
            setmodalToShow(false)}}
           />
</>)}
else {
    return (
        
        <Header curent={item?item.length:0} favorites={favorite?favorite.length:0}/>)
}

 
}
