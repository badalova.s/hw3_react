import logo from './logo.svg';
import './App.scss';
import Home from "./pages/home";
import Cart from "./pages/cart";
import Favorite from "./pages/favotire";

import { BrowserRouter, Routes, Route } from "react-router-dom";

function App() {
  return (  
  <BrowserRouter>
    <Routes>
        <Route index element={<Home />} />
        <Route path="cart" element={<Cart />} />
        <Route path="favotire" element={<Favorite />} />
      
    </Routes>
  </BrowserRouter>
  )
}

export default App;
